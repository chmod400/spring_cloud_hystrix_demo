package com.lee.hystrix.service;

import com.lee.hystrix.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Random;
import java.util.concurrent.Future;

/**
 * <br/>
 *
 * @author liyuanchang
 * @version V1.0
 * @email liyuanchang@chinamobile.com
 * @date 2019/6/20 15:58
 */
@Service
public class ClientService {
    Random random = new Random();

    private String url = "http://localhost:8080/";

    @Autowired
    private RestTemplate restTemplate;

    public String command1() {
        try {
            String result = restTemplate.postForObject(url + "command1", null, String.class);
            result = Utils.string2Html(result);
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }



    @Async
    public Future<String> asyncCommand1() {
        Utils.sleep(random.nextInt(300));
        AsyncResult<String> result = new AsyncResult<String>(Utils.string2Html(command1()));
        return result;
    }


}
