package com.lee.hystrix.controller;

import com.lee.hystrix.service.ClientService;
import com.lee.hystrix.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import sun.nio.ch.Net;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * <br/>
 *
 * @author liyuanchang
 * @version V1.0
 * @email liyuanchang@chinamobile.com
 * @date 2019/6/20 15:38
 */
@RestController
public class ClientController {
    @Autowired
    private ClientService service;

    @RequestMapping("/test")
    public String test() {
        return "test";
    }

    @RequestMapping("/command1")
    public String command1() {
        return service.command1();
    }

    @RequestMapping("/asyncCommand1/{num}")
    public String asyncCommand1(@PathVariable int num) {
        num = num == 0 ? 100 : num;
        List<Future<String>> futures = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            futures.add(service.asyncCommand1());
        }

        List<String> list = Utils.future2List(futures);
        return Utils.list2String(list);
    }


}
