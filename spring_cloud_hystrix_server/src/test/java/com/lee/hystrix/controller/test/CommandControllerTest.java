package com.lee.hystrix.controller.test;

import com.lee.hystrix.controller.CommandController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * <br/>
 *
 * @author liyuanchang
 * @version V1.0
 * @email liyuanchang@chinamobile.com
 * @date 2019/6/20 15:12
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class CommandControllerTest {

    @Autowired
    CommandController controller;

    @Test
    public void contextLoads() {}

    @Test
    public void testNotNull() {
        Assert.assertNotNull(controller);
    }

    @Test
    public void testCommand1() {
        System.out.println(controller.command1());
    }

    @Test
    public void testCommand1_100Times() {
        for (int i = 0; i < 100; i++) {
            System.out.println(controller.command1());
        }

    }

    @Test
    public void testCommand2_50Times() {
        for (int i = 0; i < 50; i++) {
            System.out.println(controller.command2());
        }

    }
}
