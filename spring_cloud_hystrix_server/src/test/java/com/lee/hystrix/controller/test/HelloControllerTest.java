package com.lee.hystrix.controller.test;

import com.lee.hystrix.controller.HelloController;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * <br/>
 *
 * @author liyuanchang
 * @version V1.0
 * @email liyuanchang@chinamobile.com
 * @date 2019/6/20 15:00
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class HelloControllerTest {
    @Autowired
    HelloController helloController;

    @Test
    public void contextLoads() {}

    @Test
    public void testNotNull() {
        Assert.assertNotNull(helloController);
    }

    @Test
    public void testTest() {
        System.out.println(helloController.test());
    }
}
