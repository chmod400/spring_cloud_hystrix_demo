package com.lee.hystrix.util.test;

import com.lee.hystrix.util.JsonUtil;
import com.netflix.hystrix.*;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

/**
 * <br/>
 *
 * @author liyuanchang
 * @version V1.0
 * @email liyuanchang@chinamobile.com
 * @date 2019/6/18 16:11
 */
public class JsonUtilTest {

    @Test
    public void object2JsonStr() {


        Student student = new Student();
        student.setId(1);
        student.setName("liyuanchang");
        student.setBirthday(new Date());

        String str = JsonUtil.object2JsonStr(student);

        System.out.println(str);
    }

    @Test
    public void object2JsonStr1() {
        Person person = Person.Builder.newBuilder().id(1).name("lee").birthday(new Date()).build();

        System.out.println(JsonUtil.object2JsonStr(person));
    }

    @Test
    public void object2JsonStr2() {
        HystrixCommand.Setter setter = HystrixCommand.Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("group1"))
                .andCommandKey(HystrixCommandKey.Factory.asKey("group1"))
                .andThreadPoolKey(HystrixThreadPoolKey.Factory.asKey("thread"))
                .andCommandPropertiesDefaults(HystrixCommandProperties.Setter().withExecutionTimeoutInMilliseconds(1_000));

        JsonUtil.object2JsonStr(setter);
    }

    @Test
    public void jsonStr2Obj() {
        String json = "{\"id\":1,\"name\":\"liyuanchang\",\"birthday\":1560850804072}";
        Student student = JsonUtil.jsonStr2Obj(json, Student.class);

        System.out.println(student);
    }

    @Test
    public void jsonStr2Obj1() {
        String json = "{\"id\":1,\"name\":\"lee\",\"birthday\":1560907868292}";
        Person person = JsonUtil.jsonStr2Obj(json, Person.class);

        System.out.println(person);
    }

    @Test
    public void get() {
    }

    @Test
    public void get1() {
    }

    @Test
    public void updateJsonStr() {
    }

    @Test
    public void jsonStr2GenericObj() {
    }

    @Test
    public void genericObj2JsonStr() {
    }
}