package com.lee.hystrix.util.test;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

import java.util.Date;

/**
 * <br/>
 *
 * @author liyuanchang
 * @version V1.0
 * @email liyuanchang@chinamobile.com
 * @date 2019/6/19 9:10
 */
@JsonDeserialize(builder = Person.Builder.class)
public class Person {
    private int id;
    private String name;
    private Date birthday;

    private Person(Builder builder) {
        id = builder.id;
        name = builder.name;
        birthday = builder.birthday;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getBirthday() {
        return birthday;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthday=" + birthday +
                '}';
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private int id;
        private String name;
        private Date birthday;

        private Builder() {
            System.out.println("builder constructor");
        }

        @JsonCreator
        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(int val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder birthday(Date val) {
            birthday = val;
            return this;
        }

        public Person build() {
            return new Person(this);
        }
    }
}
