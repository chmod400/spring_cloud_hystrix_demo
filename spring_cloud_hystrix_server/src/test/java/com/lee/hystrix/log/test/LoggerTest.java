package com.lee.hystrix.log.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.slf4j.ILoggerFactory;
import org.slf4j.impl.StaticLoggerBinder;

/**
 * <br/>
 *
 * @author liyuanchang
 * @version V1.0
 * @email liyuanchang@chinamobile.com
 * @date 2019/7/3 10:48
 */
@Slf4j
public class LoggerTest {

    @Test
    public void test() {
        log.info("aaa");
    }

    @Test
    public void test1() {
        log.info("exception", new RuntimeException("runtime exception"));
    }

    @Test
    public void test2() {
        log.warn("exception", new RuntimeException("runtime exception"));
    }

    @Test
    public void test3() {
        ILoggerFactory loggerFactory = StaticLoggerBinder.getSingleton().getLoggerFactory();
        System.out.println(loggerFactory);
    }
}
