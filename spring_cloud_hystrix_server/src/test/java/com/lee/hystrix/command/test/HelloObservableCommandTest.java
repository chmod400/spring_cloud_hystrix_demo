package com.lee.hystrix.command.test;

import com.lee.hystrix.command.HelloObservableCommand;
import org.junit.Test;
import rx.Observable;

import java.util.Iterator;

public class HelloObservableCommandTest {

    @Test
    public void test() throws Exception {
        HelloObservableCommand command = new HelloObservableCommand(2000, 3000, 0);

        Observable<String> observable = command.observe();
        Iterator<String> iterator = observable.toBlocking().getIterator();
        while(iterator.hasNext()) {
            System.out.println(iterator.next());
        }

//        System.in.read();
    }

    @Test
    public void test1() throws Exception {
        for (int i = 0; i < 100025; i++) {
            if (i > 30) {
                Thread.sleep(100);
            }
            HelloObservableCommand command = new HelloObservableCommand(600, 700, i);

            Observable<String> observable = command.observe();
            Iterator<String> iterator = observable.toBlocking().getIterator();
            while(iterator.hasNext()) {
                System.out.println(iterator.next());
            }
        }
    }

    @Test
    public void test2() throws Exception {
        for (int i = 0; i < 40; i++) {
            HelloObservableCommand command = null;
            if (i % 2 == 0) {
                command = new HelloObservableCommand(1000, 2500, i);
            } else {
                command = new HelloObservableCommand(30000, 0, i);
            }


            Observable<String> observable = command.observe();
            Iterator<String> iterator = observable.toBlocking().getIterator();
            while(iterator.hasNext()) {
                System.out.println(iterator.next());
            }
        }
    }
}
