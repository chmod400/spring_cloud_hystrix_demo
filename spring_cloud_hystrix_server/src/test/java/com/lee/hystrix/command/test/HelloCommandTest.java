package com.lee.hystrix.command.test;

import com.lee.hystrix.command.HelloCommand;
import com.lee.hystrix.util.ReflectionUtil;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixThreadPool;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.concurrent.ThreadPoolExecutor;

import static org.junit.Assert.*;

/**
 * <br/>
 *
 * @author liyuanchang
 * @version V1.0
 * @email liyuanchang@chinamobile.com
 * @date 2019/6/21 10:52
 */
public class HelloCommandTest {

    @Test
    public void test() {
        HelloCommand command = new HelloCommand("aaa", 100, 2000);


        String result = command.execute();
        System.out.println(result);

        result = command.execute();
        System.out.println(result);
    }

    @Test
    public void testSetter() {
        HystrixCommand.Setter setter = HystrixCommand.Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("aaa"));
        HelloCommand command = new HelloCommand(setter);
        System.out.println(command);
    }
}