package com.lee.hystrix.controller;

import com.lee.hystrix.command.HelloCommand;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <br/>
 *
 * @author liyuanchang
 * @version V1.0
 * @email liyuanchang@chinamobile.com
 * @date 2019/6/18 10:48
 */
@RestController
public class CommandController {

    @RequestMapping("/command1")
    public String command1() {
        HelloCommand command1 = new HelloCommand("group1", 2000, 300);
        return command1.execute();
    }

    @RequestMapping("/command2")
    public String command2() {
        // 容易熔断
        HelloCommand command2 = new HelloCommand("group2", 200, 1500);
        return command2.execute();
    }

    @RequestMapping("/command3")
    public String command3() {
        // 容易reject
        HelloCommand command3 = new HelloCommand("group3", 10_000, 10_000);
        return command3.execute();
    }
}
