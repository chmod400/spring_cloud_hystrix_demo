package com.lee.hystrix.command;

import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;
import com.netflix.hystrix.HystrixObservableCommand;
import rx.Observable;

import java.util.concurrent.TimeUnit;

public class HelloObservableCommand extends HystrixObservableCommand<String> {
    int i;
    int delayTime;

    public HelloObservableCommand(int timeout_milliseconds, int delayTime, int i) {
        super(HystrixObservableCommand.Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("hello"))
                .andCommandPropertiesDefaults(HystrixCommandProperties.Setter().withExecutionTimeoutEnabled(true)
                    .withExecutionTimeoutInMilliseconds(timeout_milliseconds)));
        this.delayTime = delayTime;
        this.i = i;
    }

    @Override
    protected Observable<String> construct() {
        System.out.println("construct - " + i);
        if (delayTime == 0) {
            return Observable.just("HelloObservableCommand - " + i);
        }
        return Observable.just("HelloObservableCommand - " + i).delay(delayTime, TimeUnit.MILLISECONDS);
    }

    @Override
    protected Observable<String> resumeWithFallback() {
        return Observable.just("fallback - " + i);
    }
}
