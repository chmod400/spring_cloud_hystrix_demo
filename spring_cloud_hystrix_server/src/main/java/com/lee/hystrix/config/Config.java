package com.lee.hystrix.config;

import com.lee.hystrix.aop.MetaHolderFactoryAspect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <br/>
 *
 * @author liyuanchang
 * @version V1.0
 * @email liyuanchang@chinamobile.com
 * @date 2019/6/27 15:57
 */
@Configuration
public class Config {
    @Bean
    public MetaHolderFactoryAspect metaHolderFactoryAspect() {
        return new MetaHolderFactoryAspect();
    }
}
