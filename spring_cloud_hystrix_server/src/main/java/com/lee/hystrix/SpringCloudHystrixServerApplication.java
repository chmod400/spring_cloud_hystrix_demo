package com.lee.hystrix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringCloudHystrixServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringCloudHystrixServerApplication.class, args);
    }

}
